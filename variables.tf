# 1GB = 1073741824
# centos6/7srv minimum  8589934592 8589934592
# ubuntu16 minimum      2361393152
# ubuntu 18 min

variable "instance_data" {
  description = "Please adjust the following variables to refect servers parameters "
  type        = map(string)
  default = {
    "count"           = "2"
    "vcpu"            = "2"
    "size"            = "5361393152"
    "network"         = "eno1"
    "memory"          = "2048"
    "hostname"        = "sandbox"
    "packages"        = "[mc, nano, vim, python, net-tools, rsync, curl, qemu-guest-agent, wget]"
    "version"         = "1.0"
    "hostname_prefix" = "kvm1.mylab.local"
    "wait_for_lease"  = "true"
    "pool"            = ""
    "customer"        = ""
    "libvirt_uri"   = "qemu+ssh://forrayz@192.168.1.49/system"
  }
}

variable "user" {
  description = "Please adjust the following variables to refect servers parameters "
  type        = map(string)
  default = {
    "user-forrayz-name"       = "forrayz"
    "user-forrayz-public-key" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQpI1DUwekKZ6N3PNKovzHxPn/1J1Ic8VJ2+jyACWvCk5K3fr7xNrUKxGFP75X0/IdD6FgGrASQT+WYlaNnVc8H6erKGTpgj6FQWraNVjLVmldf8aT7HIDLQMTwJh/kqsH+sKEBAA4FUj3FwUTxKVPVFLRZoYqX/Li4hL0mHdocrwHs5T0/YhA1ovie4CuMcib0lIYK8/xWNyGZpyM1kIDR3WzkgtnGBHf1j9VnbdRhrz7wZVJH1YKgG54YuS0tdmBCzW1EYJl2DhPlZr8cSnKJWxQWAidRSv7j0PVwsJu72THdCWgye4rU29ljUU9OtU7GVUDOUSGK+xrPydGvSB5 forrayz@forrayz-ThinkPad-P50"
    "user-forrayz-groups"     = "[sudo]"
    "user-forrayz-gecos"      = "Forray Zoltán (Baxter-IT) <zforray@baxter-it.com>"

    "cloud-user-name"       = "ansible"
    "cloud-user-public-key" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQpI1DUwekKZ6N3PNKovzHxPn/1J1Ic8VJ2+jyACWvCk5K3fr7xNrUKxGFP75X0/IdD6FgGrASQT+WYlaNnVc8H6erKGTpgj6FQWraNVjLVmldf8aT7HIDLQMTwJh/kqsH+sKEBAA4FUj3FwUTxKVPVFLRZoYqX/Li4hL0mHdocrwHs5T0/YhA1ovie4CuMcib0lIYK8/xWNyGZpyM1kIDR3WzkgtnGBHf1j9VnbdRhrz7wZVJH1YKgG54YuS0tdmBCzW1EYJl2DhPlZr8cSnKJWxQWAidRSv7j0PVwsJu72THdCWgye4rU29ljUU9OtU7GVUDOUSGK+xrPydGvSB5 forrayz@forrayz-ThinkPad-P50"
    "cloud-user-groups"     = "[wheel,sudo]"
    "cloud-user-gecos"      = "Forray Zoltán (DevOPS) <forrayz@hotmail.com>"
  }
}


#DataBase Server

