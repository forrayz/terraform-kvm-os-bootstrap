resource "libvirt_cloudinit_disk" "commoninit" {
  name      = "${var.instance_data["customer"]}-${var.instance_data["pool"]}-${var.instance_data["hostname"]}${format("%02d", count.index + 1)}.${var.instance_data["hostname_prefix"]}.cloud-init.iso"
  pool      = "iso-images"
  count     = var.instance_data["count"]
  user_data = <<EOF
#cloud-config
hostname: ${var.instance_data["customer"]}-${var.instance_data["pool"]}-${var.instance_data["hostname"]}${format("%02d", count.index + 1)}.${var.instance_data["hostname_prefix"]}
groups:
  - ubuntu: [root,sys]
  - cloud-users

# Add users to the system. Users are added after groups are added.
users:
  - default
  - name: ${var.user["cloud-user-name"]}
    gecos: Trinity without glasses
    groups: ${var.user["cloud-user-groups"]}
    ssh_import_id: ${var.user["cloud-user-name"]}
    lock_passwd: false
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ${var.user["cloud-user-public-key"]}
    ssh_pwauth: True
    chpasswd: {expire: False}
    password: secret
    passwd: secret
    shell: /bin/bash

  - name:           ${var.user["user-forrayz-name"]}
    gecos:          ${var.user["user-forrayz-gecos"]}
    groups:         ${var.user["user-forrayz-groups"]}
    ssh_import_id:  ${var.user["user-forrayz-name"]}
    lock_passwd: false
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ${var.user["user-forrayz-public-key"]}
    shell: /bin/bash



  - name:           ${var.user["cloud-user-name"]}
    gecos:          ${var.user["cloud-user-gecos"]}
    groups:         ${var.user["cloud-user-groups"]}
    ssh_import_id:  ${var.user["cloud-user-name"]}
    lock_passwd: false
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      -             ${var.user["cloud-user-public-key"]}
    shell: /bin/bash


growpart:
  mode: auto
  devices: ['/']


packages: ${var.instance_data["packages"]}

package_upgrade: true

power_state:
 delay: "+1"
 mode: reboot
 message: Rebooting to get IP
 timeout: 1
 condition: True
EOF


  #network_config = "${data.template_file.test-fw-network-config.rendered}"

}
#"${element(libvirt_cloudinit_disk.cloudinit_test-fw.*.id, count.index)}"

