
resource "libvirt_volume" "instance_data-disk" {

  name           = "${var.instance_data["customer"]}-${var.instance_data["pool"]}-${var.instance_data["hostname"]}${format("%02d", count.index + 1)}.${var.instance_data["hostname_prefix"]}.qcow2"
  base_volume_id = libvirt_volume.ubuntu18.id
  pool           = "default"
  size           = var.instance_data["size"]
  count          = var.instance_data["count"]
}

resource "libvirt_volume" "ubuntu18" {
  name = "${var.instance_data["customer"]}-${var.instance_data["pool"]}-${var.instance_data["hostname"]}.${var.instance_data["hostname_prefix"]}-MasterVolume.qcow2"
  pool = "default"
  #source = "http://192.168.254.1/cloud-images/bionic-server-cloudimg-amd64.img"
  source = "http://192.168.17.94/cloud-images/bionic-server-cloudimg-amd64.img"
}


